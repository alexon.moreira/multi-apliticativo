import React from 'react';
import styled from 'styled-components';

export const ButtonRemoverLanche = (props) => {
    return (
        <Button onClick={props.onClick}>
            <SVGRemover width={props.width} height={props.height} />
        </Button>
    );  
};

const SVGRemover = (props) => (
  <SVG viewBox="0 0 60 60"  width={props.width? props.width : "60"} height={props.height? props.height : "60"} xmlns="http://www.w3.org/2000/svg">     
    <circle className="circulo" cx="30" cy="30" r="30" />
    <rect className="negativo" x="10" y="25" width="40" height="8" ry="4" />
  </SVG>
);

// styled Button 
const Button = styled.button`
  display:flex;  
  position:absolute;
  background-color:transparent;
  border:none;    
  opacity:0.6;
  bottom:10px;
  left:30px;
  z-index:2;

  &::before {
    content: '';
	  position: absolute;
    width: 90px;
	  height: 90px;
	  top: 50%;
	  left: 50%;
	  margin: -45px 0 0 -45px;
	  border-radius: 50%;
		opacity: 0;
	  pointer-events: none;
    z-index:1;    
  }

  &::after {
    content: '';
	  position: absolute;
    width: 60px;
	  height: 60px;    
	  top: 50%;
	  left: 50%;
	  margin: -30px 0 0 -30px;
	  border-radius: 50%;	  
	  opacity: 0;
	  pointer-events: none;
    z-index:1;
  }

  &::before, &::after {box-shadow: 0 0 0 3px rgba(219, 10, 91, 0.9)}

  &:active::before {animation: anim-effect-ivana-after 0.3s forwards} 
  &:active::after  {animation: anim-effect-ivana-before 0.3s forwards} 
  
  &:active {outline: none; animation: anim-effect-ivana-after 0.5s forwards}
  &:focus {outline: none; border: none}

  @keyframes anim-effect-ivana-before {
	  0% {opacity: 1; transform: scale3d(0.5, 0.5, 1)}
	  100% {opacity: 0.5; transform: scale3d(1.1, 1.1, 1)}
  }

  @keyframes anim-effect-ivana-after {
	  0% {opacity: 0; transform: scale3d(0.5, 0.5, 1)}
	  50%, 100% {opacity:0.5; transform: scale3d(1.2, 1.2, 1)}
  }
`

// Styled SVG
const SVG = styled.svg` 
  & .circulo {fill:#f00}
  & .negativo {fill:#fff}
`