import React from 'react';
import styled from 'styled-components';

export const Spinner = () => {
    return (
        <ContainerSpinner>
            <SVGSpinner />
        </ContainerSpinner>
    );
};

const SVGSpinner = () => {
    return (
        <SVG viewBox="0 0 50 50">
            <circle className="circulo" cx="25" cy="25" r="20" fill="none" strokeWidth="4" />
        </SVG>
    );
};

// Styled Spinner 
const ContainerSpinner = styled.div`
    display:flex;
    background-color:gray;
    opacity:0.9;
    justify-content:center;
    align-items:center;
    position:fixed;
    width:100%;
    height:100%;
    z-index:1000;
`

// Styled SVG
const SVG = styled.svg`    
    margin: -25px 0 0 -25px;
    width: 50px;
    height: 50px;
    animation: rotate 2s linear infinite;
  
    & .circulo {
        stroke: #5652BF;
        stroke-linecap: round;
        animation: dash 1.5s ease-in-out infinite;
    }
  
    @keyframes rotate {
        100% {
            transform: rotate(360deg);
        }
    }

    @keyframes dash {
        0% {
            stroke-dasharray: 1, 150;
            stroke-dashoffset: 0;
        }
        50% {
            stroke-dasharray: 90, 150;
            stroke-dashoffset: -35;
        }
        100% {
            stroke-dasharray: 90, 150;
            stroke-dashoffset: -124;
        }
    }
`;