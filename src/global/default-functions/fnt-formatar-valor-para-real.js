export function fntFormatarValorParaReal(valor) {
    valor = valor.toFixed(2).replace(".",",");
    let result = valor.toLocaleString('pt-BR'); 
    return result;
}