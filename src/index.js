import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import {createGlobalStyle} from 'styled-components/macro';
import AppBifao from './bifaosteakhouse/components/app';
import * as serviceWorker from './serviceWorker';

// Styled Index
import facinatefont from './global/fonts/facinate.ttf';
import nunitoFont from './global/fonts/nunito.ttf';
import digitalFont from './global/fonts/digital.woff';
import eastFont from './global/fonts/east.ttf';
import pacificoFont from './global/fonts/pacifico.ttf';

const GlobalStyle = createGlobalStyle`
    html, body {position:fixed; width:100vw; height:100vh; margin:0px; background-color: #AD4A39; background-image: radial-gradient(ellipse farthest-side at 50% 50%, #FFF212, #F5874F, #8B2D2E 130)}
	@font-face {font-family:'digital'; src: url('${digitalFont}') format('opentype')}  
    @font-face {font-family:'facinate'; src: url('${facinatefont}') format('opentype')}
    @font-face {font-family:'nunito'; src: url('${nunitoFont}') format('opentype')}
    @font-face {font-family:'east'; src: url('${eastFont}') format('opentype')}
    @font-face {font-family:'pacifico'; src: url('${pacificoFont}') format('opentype')}
`

ReactDOM.render(
    <Fragment>
        <GlobalStyle />
        <AppBifao />        
    </Fragment>,
    document.getElementById('root')
);
serviceWorker.unregister();