import React from 'react';
import styled from 'styled-components/macro';
import {IconUser} from '../../global/default-svg-icons/index';
//import {fntFormatarValorParaReal} from '../../global/default-functions/index';

export const Header = (props) => {    
    //const [valorTotal] = useState(fntFormatarValorParaReal(0.00));    
    return (
        <HeaderContainer>
            <LogoContainer>
                <ButtomLogo><Img src={props.logo} /></ButtomLogo>
            </LogoContainer>
            <UsuarioContainer>
                <IconUser width="30px" fill="green" />       
            </UsuarioContainer>
            <ContainerCaixaPag>
                <CaixaPag>
                    <HeaderTexto>0000</HeaderTexto>
                </CaixaPag>            
            </ContainerCaixaPag>
        </HeaderContainer> 
               
    )
}

// Styled Header 
const HeaderContainer = styled.div`    
    position:fixed;
    z-index:3;
    display:flex;    
    flex-flow:row nowrap;
    justify-content: center;
    align-items: space-between;    
    width:100%;
    height:60px;
    top:0px;
    overflow:hidden;
`
const LogoContainer = styled.section`
    display:flex;
    flex:1;
    align-items:center;
    justify-content:center;
`
const UsuarioContainer = styled.section`
    display:flex;
    align-items:center;
    justify-content:center;
    height:100%;
    width:100%;   
`
const ContainerCaixaPag = styled.section`
    display:flex;    
    align-items:center;
    justify-content:center;    
    width:300px;
`
const CaixaPag = styled.section`
    position:relative;
    display:flex;    
    align-items:center;
    justify-content:center;
    width:85%;
    height:35px;
    background-image: radial-gradient(ellipse farthest-side at 50% 50%, #11F4FD, #4DB7D7 400%);
    border-radius:5px;
    border:solid 3px #8B2D2E;

    &::after {
        content: "";               
        background-image: url(${require(`../statics/imgs/bg1-caixapag.png`)});        
        background-position: center center;
        background-size: cover;
        position:absolute;
        top:-1px;
        width:100%;
        height:30px;
        z-index:1; 
    }  
`

const HeaderTexto = styled.span`
    display:block;
    width:100%;
    text-align:right;
    line-height:60px;    
    padding-right:10px;                
    color:#3E4095;
    font-size:1.9em;    
    font-family: 'digital';
    z-index:2;
`

const ButtomLogo = styled.button`
    width:120px;
    cursor:pointer;
    border:none;
    background-color: transparent;   
    
    &:active {
        outline: none;    
        border: none;
    }

    &:focus {
        outline: none;
        border: none;
        transform: scale(1);
    }

    &:hover {
        transform: scale(1.2);
    }
`

const Img = styled.img`
    margin-top:10px;
    max-width:85%;
`