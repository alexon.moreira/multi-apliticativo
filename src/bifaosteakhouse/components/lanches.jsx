import React, {useState} from 'react';
import styled from 'styled-components';
//import jsonLanches from '../statics/json/lanches.json';
import bgLeft from '../statics/imgs/bg-left.png';
import bgRight from '../statics/imgs/bg-right.png';
import bgTop from '../statics/imgs/bg-top.png';
import bgBottom from '../statics/imgs/bg-bottom.png';
import {ButtonAdicionarLanche, ButtonRemoverLanche, ButtonMarque} from '../../global/default-components/index';
//import {fntFormatarValorParaReal} from '../../global/default-functions/index';

export const Lanches = (props) => {
    const [state_object_style_ingredientes, fnt_object_style_ingredientes] = useState({animation:'none', color:'black'});
    //console.log(stateStrokeDashOffSet);
    //const [stateLanches] = useState(jsonLanches);

    //"ingredientes":[["ingrediente:Pão com gergelim|opcional:nao|ativo:sim"],["ingrediente:2 carnes artesanais|opcional:nao|ativo:nao"],["ingrediente:Bacon|opcional:nao|ativo:nao"],["ingrediente:queijo mussarela derretido|opcional:sim|ativo:nao"],["ingrediente:molho do Chef|opcional:sim|ativo:nao"]],
     
    /*
    function fntRetornaImagem(valor, img) {      
        return (valor === 0 ? require(`../statics/imgs/lanches/${img}`) : img);        
    }

    function fntMapearIngredientes(arrayIngredientes) {
        let arryaItens = arrayIngredientes[0].split(',').length;        
        let array = [];
        for(let i=0; i<=arryaItens-1; i++) {            
            let res = arrayIngredientes[0].split(',')[i]
            array.push(res);      
        }        
        const IngredientesComposicao = (array.map((res, idx) => <IngredienteDescricao key={idx}>{res}</IngredienteDescricao>));
        return IngredientesComposicao;
    }

    function fntRetornaPrecoNomalOuPromocao() {
        return  
    }

    function fntValorNomalValorPromocao(precoNormal, precoPromocao){
        const valor = if(precoPromocao > 0 ? fntFormatarValorParaReal(precoPromocao) : fntFormatarValorParaReal(precoNormal));    
        return <Sifrao>R$ <LancheValor>{valor}</LancheValor></Sifrao>;
    }
    */

    /*{
                stateLanches.map((res, idx) => (
                        <LancheContainer key={idx} cor=''>
                            <LancheDescricao key={6}>{res.descricaoLanche}</LancheDescricao>
                            <ContainerImg>{<Img src={fntRetornaImagem(1, res.imagens[1])} />}
                                {fntRetornaPrecoNomalOuPromocao(res.ValorNormal, res.valorPromocao)}
                            </ContainerImg>                           
                            <IngredientesContainer>
                                <SubTitulo>Ingredientes</SubTitulo>                               
                                    {fntMapearIngredientes(res.ingredientes)}                               
                            </IngredientesContainer>
                        </LancheContainer>
                    ))
               
    }*/

    function fntNaoQueroIngrediente() {        
        let anima = state_object_style_ingredientes.animation; 
        if(anima === 'none'? fnt_object_style_ingredientes({animation:'anima-riscar-ingredientes', color:'Gainsboro'}) : fnt_object_style_ingredientes({animation:'none', color:'black'}));
    };

    return (        
        <Container>            
           <ContainerLanche>                
                <DescricaoLanche key={1}>Vigorous Bifão</DescricaoLanche>                                        
                <ContainerImgLanche>
                <PrecoLanche><SifraoPrecoLanche>R$</SifraoPrecoLanche>12,50</PrecoLanche>
                    <ImgLanche src={require('../statics/imgs/lanches/03.png')} />
                    <ButtonAdicionarLanche />
                    <ButtonRemoverLanche />
                </ContainerImgLanche>
                <ContainerIngredientes> 
                    <ContainerImgLeft><ImgBordaLeftRight src={bgLeft} alt='Imagem bordar left Ingredientes' /></ContainerImgLeft>
                    <ContainerImgRight><ImgBordaLeftRight src={bgRight} alt='Imagem bordar Right Ingredientes' /></ContainerImgRight>
                    <ContainerImgTop><ImgBordaTopBottom src={bgTop} alt='Imagem bordar Top Ingredientes' /></ContainerImgTop>
                    <ContainerImgBottom><ImgBordaTopBottom src={bgBottom} alt='Imagem bgBottom Top Ingredientes' /></ContainerImgBottom>         
                    <SubTitulo>Ingredientes</SubTitulo>
                    <DescricaoIngrediente>Pão com gergelim</DescricaoIngrediente>
                    <DescricaoIngrediente>2 carnes artesanais</DescricaoIngrediente>
                    <DescricaoIngrediente>Bacon I</DescricaoIngrediente>
                    <DescricaoIngrediente>Bacon II</DescricaoIngrediente>
                    <ButtonMarque onClick={fntNaoQueroIngrediente} style={state_object_style_ingredientes}>Cebola Frita</ButtonMarque>                        
                    <ButtonMarque onClick={fntNaoQueroIngrediente} style={state_object_style_ingredientes}>queijo mussarela derretido</ButtonMarque>                        
                    </ContainerIngredientes>
            </ContainerLanche>

            <ContainerLanche>
                    <DescricaoLanche key={1}>Bif Mega bacon</DescricaoLanche>                                        
                    <ContainerImgLanche>                        
                        <PrecoLanche><SifraoPrecoLanche>R$</SifraoPrecoLanche>9,90</PrecoLanche>
                        <ImgLanche src={require('../statics/imgs/lanches/06.png')} />
                        <ButtonAdicionarLanche />
                        <ButtonRemoverLanche />
                    </ContainerImgLanche>                        
                    <ContainerIngredientes>
                        <ContainerImgLeft><ImgBordaLeftRight src={bgLeft} alt='Imagem bordar left Ingredientes' /></ContainerImgLeft>
                        <ContainerImgRight><ImgBordaLeftRight src={bgRight} alt='Imagem bordar Right Ingredientes' /></ContainerImgRight>
                        <ContainerImgTop><ImgBordaTopBottom src={bgTop} alt='Imagem bordar Top Ingredientes' /></ContainerImgTop>
                        <ContainerImgBottom><ImgBordaTopBottom src={bgBottom} alt='Imagem bgBottom Top Ingredientes' /></ContainerImgBottom>        
                        <SubTitulo>Ingredientes</SubTitulo>
                        <DescricaoIngrediente>Pão com gergelim</DescricaoIngrediente>
                        <DescricaoIngrediente>2 carnes artesanais</DescricaoIngrediente>
                        <DescricaoIngrediente>Bacon</DescricaoIngrediente>        
                        <DescricaoIngrediente>Pão com gergelim</DescricaoIngrediente>
                        <DescricaoIngrediente>2 carnes artesanais</DescricaoIngrediente>
                        <DescricaoIngrediente>Bacon</DescricaoIngrediente>
                        <ButtonIngrediente>Queijo mussarela derretido</ButtonIngrediente>                        
                        <ButtonIngrediente>queijo mussarela derretido</ButtonIngrediente>
                        <ButtonIngrediente>molho do Chef</ButtonIngrediente>
                        <ButtonIngrediente>Molho do Chef</ButtonIngrediente>
                        <ButtonIngrediente>Queijo mussarela derretido</ButtonIngrediente>                        
                        <ButtonIngrediente>queijo mussarela derretido</ButtonIngrediente>
                        <ButtonIngrediente>molho do Chef</ButtonIngrediente>
                        <ButtonIngrediente>Molho do Chef</ButtonIngrediente>
                    </ContainerIngredientes>
            </ContainerLanche>

            <ContainerLanche>
                    <DescricaoLanche key={1}>kids Chicken Burger</DescricaoLanche>                                        
                    <ContainerImgLanche>
                    <PrecoLanche><SifraoPrecoLanche>R$</SifraoPrecoLanche>6,85</PrecoLanche>
                        <ImgLanche src={require('../statics/imgs/lanches/04.png')} /> 
                        <ButtonAdicionarLanche />
                        <ButtonRemoverLanche />
                    </ContainerImgLanche>                        
                    <ContainerIngredientes>
                        <ContainerImgLeft><ImgBordaLeftRight src={bgLeft} alt='Imagem bordar left Ingredientes' /></ContainerImgLeft>
                        <ContainerImgRight><ImgBordaLeftRight src={bgRight} alt='Imagem bordar Right Ingredientes' /></ContainerImgRight>
                        <ContainerImgTop><ImgBordaTopBottom src={bgTop} alt='Imagem bordar Top Ingredientes' /></ContainerImgTop>
                        <ContainerImgBottom><ImgBordaTopBottom src={bgBottom} alt='Imagem bgBottom Top Ingredientes' /></ContainerImgBottom>
                        <SubTitulo>Ingredientes</SubTitulo>
                        <DescricaoIngrediente>Pão com gergelim</DescricaoIngrediente>
                        <DescricaoIngrediente>2 carnes artesanais</DescricaoIngrediente>
                        <DescricaoIngrediente>Bacon</DescricaoIngrediente>        
                        <DescricaoIngrediente>Pão com gergelim</DescricaoIngrediente>
                        <DescricaoIngrediente>2 carnes artesanais</DescricaoIngrediente>
                        <DescricaoIngrediente>Bacon</DescricaoIngrediente>
                        <ButtonIngrediente>Queijo mussarela derretido</ButtonIngrediente>                        
                        <ButtonIngrediente>queijo mussarela derretido</ButtonIngrediente>
                        <ButtonIngrediente>molho do Chef</ButtonIngrediente>
                        <ButtonIngrediente>Molho do Chef</ButtonIngrediente>
                        <ButtonIngrediente>Queijo mussarela derretido</ButtonIngrediente>                        
                        <ButtonIngrediente>queijo mussarela derretido</ButtonIngrediente>
                        <ButtonIngrediente>molho do Chef</ButtonIngrediente>
                        <ButtonIngrediente>Molho do Chef</ButtonIngrediente>
                    </ContainerIngredientes>
            </ContainerLanche>     
        </Container>
    )   
}

const Container = styled.div`    
    display:flex;   
    flex-flow:column nowrap;
    align-items: center;
    justify-content: flex-start;    
    width:100vw;
    height:100vh;   
    background-color: #AD4A39;
    background-image: radial-gradient(ellipse farthest-side at 50% 50%, #FFF212, #F5874F, #8B2D2E 130%);    
    overflow:auto;
 
    &::after {
        content: "";               
        background-image: url(${require(`../statics/imgs/bg4.png`)});
        background-size: cover;
        background-repeat: no-repeat;
        background-position: fixed;
        opacity: 0.9;
        position: fixed;
        z-index: 1;
        width:100vw;
        height:100vh; 
    }  
`
const ContainerLanche = styled.div`   
    z-index: 2;   
    display:flex;   
    flex-flow:column wrap;
    align-items:center;
    justify-content:flex-start;  
    max-width:600px;   
    padding-top:10px;
    padding-bottom:20px;
    border:solid 2px #8B2D2E;
    margin:5px;
    border-radius:10px;    

    &:nth-child(1) {
        margin-top:65px;
    } 
`

const ContainerIngredientes = styled.div` 
    position:relative;
    display:flex;
    flex-flow:column wrap;
    align-items:center;
    justify-content:center;
    width:85%;    
    background-image: radial-gradient(ellipse farthest-side at 50% 50%, #FFFFFF 50%, #FFFFFF 80%, #E6E7E8);
    padding:10px;
    border-radius:3px;
    margin-top:15px;    
`

const DescricaoLanche = styled.span`
    position:relative;    
    display:flex;
    width:100%;
    align-items:center;
    justify-content:center;
    font-size:1.7em;
    font-family: 'pacifico', Arial;
    color:#8B2D2E;    
  
    &::after {
        content: "";
        position:absolute;
        border-bottom:solid 2px #FCD433;
        width:90%;
        top:33px;
        height:3px;
    }
`

const ContainerImgLanche = styled.div`
    position:relative;
    display:flex;
    justify-content:center;
    align-items:center;
    width:100%;
    margin-top:20px;
`
const PrecoLanche = styled.span`    
    position:absolute;
    display:flex;
    align-items:center;
    justify-content:flex-end;
    padding:5px;
    font-size:1.9em;
    font-family: 'east', 'Arial';
    width:100px;
    height:30px;
    top:-15px;
    left:40%;
    padding-right:10px;
    transform: rotate(-10deg);
    z-index:1;
    color:orange; 

    &::after {
        content: "";               
        background-image: url(${require(`../statics/imgs/bg1-preco-lanche.png`)});        
        background-position: center center;
        background-size: cover;
        position:absolute;
        top:5px;
        left:2px;
        width:100%;
        height:30px;
        z-index:-1; 
    }  
`

const SifraoPrecoLanche = styled.i`
    position:absolute;
    color:red;
    font-size:0.5em;
    bottom:7px;
    left:35px;
`

const ImgLanche = styled.img`        
    max-width:70%;    
`
const SubTitulo = styled.span`
    display:block;
    position:relative;
    display:flex;
    width:100%;
    align-items:center;
    justify-content:center;
    font-size:1.4em;
    font-family: 'pacifico', Arial;
    color:#8B2D2E;    
    line-height:30px;
    margin-bottom:10px;
  
    &::after {
        content: "";
        position:absolute;
        border-bottom:solid 2px #FCD433;
        width:60%;
        top:25px;
        height:3px;
    }  
`

const DescricaoIngrediente = styled.span`
    display:block;
    position:relative;
    line-height:15px;   
    font-size:1em;    
    font-family:'pacifico', 'east', 'nunito', Arial;     
    color:black;
    margin:1px;       
    padding:3px;
    text-align:left;    
    width:95%;    

    &::after {
        content: "";
        position:absolute;
        border-bottom:solid 1px #D2D3D5;
        width:100%;
        left:0px;
        top:17px;
        line-height:15px;
    }    
`

const ButtonIngrediente = styled.button`
  display:flex;
  justify-content:flex-start;
  align-items:center;
  position:relative;
  --pointer-events: none;
  background-color:transparent;    
  font-size:1em;    
  font-family:'pacifico', 'east', 'nunito', Arial;
  border: none;    
  color:black;
  text-align:left;    
  width:95%;

  &:active {outline: none}
  &:focus {outline: none; border: none}

    &::after {
        content: "";
        position:absolute;
        width:100%;               
        left:0px;
        bottom:5px;
        border-bottom:solid 1px #D2D3D5;           
    } 
`

const ContainerImgLeft = styled.div`
    position:absolute; 
    min-height:100%;      
    left:-10px;
    top:0px;
    --border:solid 1px red; 
`
const ContainerImgRight = styled.div`
    position:absolute; 
    min-height:100%;      
    right:7px;
    top:0px;     
`

const ImgBordaLeftRight = styled.img`
    position:absolute;
    width:15px;
    height:100%;
`

const ContainerImgTop = styled.div`
    position:absolute; 
    width:100%;
    height:15px;
    left:0px;
    top:-14px;    
`

const ContainerImgBottom = styled.div`
    position:absolute; 
    width:100%;
    height:15px;
    left:0px;
    bottom:-14px;    
`

const ImgBordaTopBottom = styled.img`
    position:absolute;
    width:100%;
    height:15px;
`