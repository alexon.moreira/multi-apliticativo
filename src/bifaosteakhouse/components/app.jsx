import React from 'react';
import styled from 'styled-components';
import LogoBifao from '../statics/imgs/logo-bifao.svg';
import {Header} from './header';
import {Lanches} from './lanches';

export default function AppBifao() {    
    return (                    
        <ContainerApp>
            <Header logo={LogoBifao} />            
            <Lanches />
        </ContainerApp>
    )
}

const ContainerApp = styled.div`
    display:flex;
    flex:1;
    flex-flow: column nowrap;
    align-items: center;
    justify-content: center;
    width:100vw;
    height: 100vh;
}
`