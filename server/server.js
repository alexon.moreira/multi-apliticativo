import fs        from 'fs';
import path      from 'path';
import http2     from 'http2';
import mimeTypes from './mime-types.json';

const key     = fs.readFileSync('./security/cert.key'); 
const cert    = fs.readFileSync('./security/cert.pem');  
const server  = http2.createSecureServer({key, cert, allowHTTP1:true});

const {HTTP2_HEADER_PATH, HTTP2_HEADER_METHOD, HTTP_STATUS_NOT_FOUND, HTTP_STATUS_INTERNAL_SERVER_ERRO, HTTP2_HEADER_STATUS} = http2.constants;
const PUBLIC_FILES = __dirname+'/public';

function fntRespondErrorStream(err, stream) {
  if(err.code === 'ENOENT') {
      stream.respond({':status':HTTP_STATUS_NOT_FOUND});
  }
  else {
    stream.respond({'status':HTTP_STATUS_INTERNAL_SERVER_ERRO});
  }
  stream.end();
}

function fntContetType(file_extensao) {  
  let contentMimeTypes = '';  
  if(file_extensao !== null) {
     let contentMimeTypes = mimeTypes.filter(res => res.extensao === file_extensao);          
    
    for(let i of contentMimeTypes){
      contentMimeTypes = i.contentType;
      return contentMimeTypes
    }    
  }   
}

server.on('error', (err) => console.error(err));

server.on('stream', (stream, headers) => {
  const reqPath   = headers[HTTP2_HEADER_PATH];  
  const reqMethod = headers[HTTP2_HEADER_METHOD];
  const pathFile  = path.join(PUBLIC_FILES, reqPath);

  console.log(reqPath); 
 
  if(reqPath === '/') {
    const file = `${pathFile}index.html`;
    stream.respondWithFile(file, {'content-type':'text/html', ':status':200}, {onError: (err) => {fntRespondErrorStream(err, stream)}});
  }

  if(reqPath !== '/') {    
    let contentTypeValue = fntContetType(path.extname(pathFile).split('.')[1] || null);
      const file = `${pathFile}`;
      stream.respondWithFile(file, {'content-type':`${contentTypeValue}`, ':status':200}, {onError: (err) => {fntRespondErrorStream(err, stream)}});            
  }

});
 
server.listen(8090, () => console.log('Servidor HTTPS Rodando na porta 8090'));